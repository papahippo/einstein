In the good (bad, ugly?) ol days of 200k per side 3" floppy disks
and no networking, it was quite difficult to keep track of what
was on what floppy. Also automated version control was non-existent.
The lack of date/time info in Crystal DOS (effectively CP/M) directories
made even manual organization (which I in particular find challenging in
most fields of human endeavour) not easy.

So I'm trying to get a better grip on this in this 'software
archeaology' stage of operations by uploading source files and
per-side per-disk directory information to a local PC (and then
onto the cloud in the form of 'gitlab' where appropriate.
 
The python script 'collate_log.py' in this diretory does roughly what
the name suggests.

