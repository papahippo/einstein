#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" help to locate stuff on all these 2x192kbyte diskettes!
The raw log 'flops.log' was obtained by connecting a PC running (e.g.)
minicom via a serial line to an Einstein TC01 running (X)basic
and entering (on the TC01) the command line: PRINT#2:DIR
"""
import sys, os
#from _collections import OrderedDict

def sort_criterion(file_dict):
    return file_dict['ext3']+file_dict['body8']

def sort_criterion2(item_):
    first_file_dict = item_[1][0]
    return first_file_dict['ext3']+ first_file_dict['body8']


def main():
    if ('--help' in sys.argv):
        print("syntax...  collate_log [log_name [ext]]  "
              "... where ext if specified should be upper case and begin with a '.'.")
        sys.ext(0)
    prog_name = sys.argv.pop(0)
    log_name = sys.argv.pop(0) if sys.argv else 'flops.log'
    only_ext = sys.argv.pop(0) if sys.argv else None
    print(f"running '{prog_name}' on '{log_name}' in '{os.getcwd()}'")

    flop_sides = {}
    all_files = []
    files_by_name = {}
    current_flop = current_file = None

    for line in open(log_name, 'r'):
        lin = line[:-1]  # ditch the new-line at the end of the line.
        if not lin:
            continue  # balnk lines are just for readability in raw log.
        if line[0] in (' ', '\t'):
            if not current_flop:
                continue  # ignore heading stuff at top of file
            # print(f"filenames line = <<{lin}>>")
            chunks = lin.split(':')[1:]  # ditch the space at start of line
            for chunk in chunks:
                prot1 = chunk[0]
                protected = prot1=='*'
                body8, ext3 = chunk[1:].split('.')
                ext3 = ext3[:3]
                #print (f'<{ext3}>')
                body = body8.rstrip()
                ext = '.' + ext3.rstrip()  # include '.' as part of extension
                filename = body + ext
                current_file = dict(
                    filename=filename,
                    protected=protected,
                    on_diskette=current_flop,
                    body8=body8,
                    ext3=ext3,
                    prot1=prot1,
                )
                current_flop['files'].append(current_file)
                all_files.append(current_file)
                files_by_name.setdefault(filename, []).append(current_file)
        else:
            lin = lin.replace(':', '')  # ditch the (inconsistently used colon!)
            diskette_name = lin[:3]
            diskette_info = lin[3:]
            # print(f"diskette name={diskette_name}   info={diskette_info}")
            if diskette_name in flop_sides:
                print(f"*** warning: reusing name {diskette_name} - old stats will be lost! ****")
            flop_sides[diskette_name] = current_flop = dict(
                diskette_name=diskette_name,
                info=diskette_info,
                files=[])
    print(f"{len(flop_sides)} diskette-side items ({(1+len(flop_sides))//2} physical diskettes?)")
    # print (sorted(flop_sides.keys()))
    # for file_dict in sorted(all_files, key=sort_criterion):
    for key_ , candidates in sorted(files_by_name.items(), key=sort_criterion2):
        how_many = f"({len(candidates)})"
        where = ', '.join([candy['on_diskette']['diskette_name'] for candy in candidates])
        print(f"{key_:14}{how_many:6}{where}")



if __name__ == "__main__":
    main()
