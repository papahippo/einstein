#!/usr/bin/python3
import sys, os, serial
from struct import pack, unpack
from collections import OrderedDict

START_CHAR = b'\x81'

def hex_bytes(bb, joiner=':'):
    return joiner.join([f'{b:02x}' for b in bb])

def masked_str(bb, mask=0x7f, c_between=''):
    return c_between.join([chr(b & mask) for b in bb])

class XXDOS_server:

    def vprint(self, verbosity, *pp, **kw):
        if self.verbosity >= verbosity:
            print(*pp, **kw)

    def __init__(self):
        program_name = sys.argv.pop(0)
        print(f"runing: {program_name}")
        dirs_to_serve = []
        self.verbosity = 0
        self.baud_rate = 9600
        while sys.argv:
            arg = sys.argv.pop(0)
            if arg[0] != '-':
                dirs_to_serve.append(arg)
                continue
            if arg in('-v', '--verbose'):
                self.verbosity += 1
                continue
            if arg in('-b', '--baud'):
                self.baud_rate = int(sys.argv.pop(0))
                continue
            if arg not in ('-h', '--help)'):
                print(f"not understood: '{arg}")
            print(f"syntax: {program_name}   [ -v | --verbosity] [-h | --help]    ")
            sys.exit(0)

        if not dirs_to_serve:
            dirs_to_serve = ['.',]  # default = serve current directory as drive 5 (4:....)
        self.openFileDict = {}
        self.driveDict = OrderedDict(enumerate(dirs_to_serve, 5))
        self.serial_port = serial.Serial('/dev/ttyUSB0', self.baud_rate, rtscts=True,
                                         timeout=10, stopbits=serial.STOPBITS_TWO)
        self.func_handlers = {   14: self.xXDOS_selectDrive,
                                 15: self.xXDOS_open,
                                 16: self.xXDOS_close,
                                 17: self.xXDOS_getDirEntry,
                                 18: self.xXDOS_getDirEntry,
                                 20: self.xXDOS_readSeq,
                                 21: self.xXDOS_writeSeq,
                                 22: self.xXDOS_create,
                                 35: self.xXDOS_getFileSize,
                                 }

    def serve(self):
        # ultimately, this may involve a separate serial thread or at the very
        # least some kind of 'select' call; but for now KISSS is my watchword!
        print(*[f" {num} ({num-1}:...) = {path}" for num, path in self.driveDict.items()], sep='\n')
        while True:
            try:
                start_char = self.serial_port.read(1)
                if  start_char != START_CHAR:
                    print(f"{start_char}? ")
                    continue
                self.vprint (2,"hurrah! we got start char!")
                regC, regD, regE = self.serial_port.read(3)
                self.vprint(1, f"regC={regC}; regD=0x{regD:02X}; regE=0x{regE:02X}")
                bytes_back = self.func_handlers.get(regC, self.xXDOS_bad_func)(regC, regD, regE)
                self.vprint(2, f"{len(bytes_back)} bytes back:", bytes_back)
                self.serial_port.write(bytes_back)
            except KeyboardInterrupt:
                self.serial_port.write(b'\x1b'*150)
                raise

    def xXDOS_bad_func(self, regC, regD, regE):
        self.vprint(0, f"error: we don;'t expect to be called on to handle XDOS function {regC}.")
        return b'\xfd'

    def get_fcb_address_and_content(self, regC, regD, regE):
        return regD*256+regE, self.serial_port.read(36)

    def xXDOS_selectDrive(self, regC, regD, regE):
        self.vprint(1, f"select drive {regE} - important if we get drive 0 reference later.")
        self.defaultDrive = regE
        return b'\x00'

    def xXDOS_open(self, regC, regD, regE):
        fcb_address, fcb_content = self.get_fcb_address_and_content(regC, regD, regE)
        self.vprint(2, fcb_address, fcb_content)
        driveNr = fcb_content[0] or self.defaultDrive
        short_name = (masked_str(fcb_content[1:9]).rstrip().replace('/','\\')
                      + '.' + masked_str(fcb_content[9:12]).rstrip())
        try:
            filename = os.path.join(self.driveDict[driveNr], short_name)
        except KeyError:
            print(f"error: no mapping for drive {driveNr}")
            return b'\xfe'
        try:
            mode = 'rb' if regC == 15 else 'wb'
            myFile = open(filename, mode) # treat all files as as binary
        except FileNotFoundError:
            print(f"file not found: {filename}")
            return b'\xff'
        self.openFileDict[fcb_address] = myFile, filename
        self.vprint(1, f"file '{filename} opened with mode '{mode}', associated with FCB 0x{fcb_address:04x}")
        return b'\x00'

    xXDOS_create = xXDOS_open  # use passed regC to differentiate these calls

    def xXDOS_close(self, regC, regD, regE):
        fcb_address, fcb_content = self.get_fcb_address_and_content(regC, regD, regE)
        f, name = self.openFileDict[fcb_address]
        f.close()
        self.vprint(1, f"closed file assciated with 0x{fcb_address:04X} ({name})")
        del self.openFileDict[fcb_address]
        return b'\x00'

    def xXDOS_readSeq(self, regC, regD, regE):
        fcb_address, fcb_content = self.get_fcb_address_and_content(regC, regD, regE)
        f, name = self.openFileDict[fcb_address]
        try:
            blk128 = f.read(128)
        except (ValueError, IOError):
            return b'\xfb'  # arbitrary code for 'I/O error' and simlilar
        self.vprint(2, blk128)
        if not blk128:
            return b'\xff'  # is this the right return code for end-of-file?
        self.vprint(1, f"end of file associated with 0x{fcb_address:04X} ({name})")
        len_short = 128-len(blk128)
        if len_short:
            self.vprint(1, f"warning: (final?) block of file is {len_short} bytes short")
            blk128 += len_short*b'\x1a'  # handy if file is text file?
        return b'\x00' + blk128

    def xXDOS_writeSeq(self, regC, regD, regE):
        fcb_address, fcb_content = self.get_fcb_address_and_content(regC, regD, regE)
        f, name = self.openFileDict[fcb_address]
        blk128 = self.serial_port.read(128)
        self.vprint(2, blk128)
        try:
            f.write(blk128)
        except (ValueError, IOError):
            return b'\xfb'  # arbitrary code for 'I/O error' and simlilar
        return b'\x00'

    def xXDOS_getFileSize(self, regC, regD, regE):
        # N.B my impementation requires the file to have been opened; this is
        # probably incorrect.
        #
        fcb_address, fcb_content = self.get_fcb_address_and_content(regC, regD, regE)
        f, name = self.openFileDict[fcb_address]
        size = f.seek(0, 2)
        return pack('<L', (2*size) & 0xffffff00) # ret code, R0, R1, R2

    def xXDOS_getDirEntry(self, regC, regD, regE):
        if regC == 17:
            fcb_address, fcb_content = self.get_fcb_address_and_content(regC, regD, regE)
            driveNr = fcb_content[0] or self.defaultDrive
            self.dir_and_index = (os.listdir(self.driveDict[driveNr]), 0)
        dir_, index_ = self.dir_and_index
        print(f"index={index_}; len(dir_)={len(dir_)};")
        if index_ >= len(dir_):
            return b'\xff'  # correct(?) code for ''end-of-directory'
        dir_code = index_ & 3
        answer = bytes((dir_code,))
        if dir_code == 0:
            for index_plus in range(index_, index_+4):
                if index_plus >= len(dir_):
                    name_part = b'\00'*12
                else:
                    stem_, ext_ = os.path.splitext(dir_[index_plus])
                    name_part = (b'\x00'
                                + bytes('%-8s%-3s' % (stem_, ext_[1:]), 'utf8'))
                answer += (name_part + 20*b'\x00')
        index_ += 1
        self.dir_and_index = dir_, index_
        return answer

if __name__=="__main__":
    XXDOS_server().serve()
