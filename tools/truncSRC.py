#!/usr/bin/env python3

from walker import Walker

class Truncator(Walker):

    name_ =  "XDOS (CP/M) fie truncator"
    myExts = ('.src',)

    def process_keyword_arg(self, a):
        if a in ('-b', '--baud'):
            self.sExtra += f'--scale {self.next_float_arg("90%")}'
            return a
        return Walker.process_keyword_arg(self, a)

    def handle_item(self, root_, item_, is_dir):
        if not Walker.handle_item(self, root_, item_, is_dir):
            return
        with open(self.full_source_name, 'r+b') as src_file:
            content = src_file.read()
            old_length = len(content)
            try:
                new_length = 1 + content.index(b'\x1a')
            except ValueError:
                self.vprint(1, f"no end-of-file (ctrl-Z); leaving {self.full_source_name} alone!" )
                return False
            self.vprint(1, f"truncating {self.full_source_name} from {old_length} to {new_length}")
            src_file.truncate(new_length)
        return True


if __name__ == '__main__':
    Truncator().main()
