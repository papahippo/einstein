#!/usr/bin/python3
import sys, serial
from struct import pack, unpack

def hex_bytes(bb, joiner=':'):
    return joiner.join([f'{b:02x}' for b in bb])

def suggest(port, stage):
    while 1:
        print(f"suggesting stage {stage}...", end=' ')
        port.write(stage)
        try:
            answer = port.read(1)
        except KeyboardInterrupt:
            print("sending break condition!")
            port.sendBreak()
            raise
        if not answer:
            print("timeout")
        else:
            print(f"answer={answer})")
        if answer==stage:
            return True  # to be embellished later?

def main():
    progr_name = sys.argv.pop(0)
    input_name = sys.argv.pop(0) if sys.argv else 'alice.dsk'
    with serial.Serial('/dev/ttyUSB0', 9600, rtscts=True,
                       stopbits=serial.STOPBITS_TWO, timeout=10) as serial_port:
        with open(input_name, 'rb') as input_file:
            disk_info_block = input_file.read(0x100)
            header_txt = disk_info_block[0:0x22]
            creator = disk_info_block[0x22:0x30]
            track_count = disk_info_block[0x30]
            side_count = disk_info_block[0x31]
            track_lengths = [0x100*size_msb for size_msb in
                             disk_info_block[0x34:0x34+side_count*track_count]]
            print(f'"{header_txt}" created by "{creator}" has {side_count} side(s) each with {track_count} tracks.')
            remaining_track_lengths = track_lengths[:]
            for side_index in range(side_count):
                print(f'side_index {side_index}')
                for track_index in range(track_count):
                    where = input_file.tell()
                    track_info_block = input_file.read(0x100)
                    tib_track_no = track_info_block[0x10]
                    tib_side_no = track_info_block[0x11]
                    tib_sector_count = track_info_block[0x15]
                    track_data_size = remaining_track_lengths.pop(0) - 0x100
                    print(
    f'@{hex(where)}track_index {track_index}; tib_track_no={tib_track_no}; '
    f'tib_side_no={tib_side_no}; track_data_size={track_data_size}'
                    )
                    tib_sector_info = track_info_block[0x18:]
                    for sector_index in range(tib_sector_count):
                        suggest(serial_port, b'\x01')
                        one_sector_info = tib_sector_info[:8]
                        tib_sector_info = tib_sector_info[8:]
                        track, side, sector_id, actual_length = unpack('<BBBxxxH', one_sector_info)
                        print(f'{sector_id}({actual_length})')
                        disk_id = 2  # hard-code to device 2 for now
                        write_cmd = pack('<cBBBBH', b'W', disk_id, side, track, sector_id, actual_length)
                        print(hex_bytes(write_cmd), end='.  ')
                        serial_port.write(write_cmd)
                        # sys.exit(0) # TEMPORARY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        write_data = input_file.read(actual_length)
                        suggest(serial_port, b'\x02')
                        serial_port.write(write_data)

                    print()
                    # print (remaining_track_lengths)
            serial_port.sendBreak()
            assert  not remaining_track_lengths

if __name__=="__main__":
    main()
