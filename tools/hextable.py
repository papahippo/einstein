#!/usr/bin/python3
from intelhex import IntelHex

"""
The aim of this script is simple - trivial even: it reads a dex file representing XDOS's
internal jump table, then writes it out in man-readable form.
This has been overtaken by events; my 'XXODS' no longer messes with this table.
By the way, trailing spaces in the lines of the hex file cause exceptions. I don't know I
they got there but it took me an age to find out they were present and doing real damage!
Code is raterh ugly. I had wanted to work with byte strings and struct.unpack but...   
"""

NUM_FUNCS=41

def main():
    ihx = IntelHex('XDOSTAB.HEX')
    bin = ihx.tobinarray()
    for funcNo in range(NUM_FUNCS):
        funcAddress = bin[2*funcNo] + 256*bin[1+2*funcNo]
        print(f"xdos{funcNo}: EQU 0{funcAddress:04X}H; Crystal DOS Function {funcNo:2}")
              # file=outfile)
if __name__=="__main__":
    main()
