#!/usr/bin/python3
from struct import pack, unpack
# out of service code. It seems to work but my binary input was corrupt.
# see 'hextable.py' for new approach.

NUM_FUNCS=41

def main():
    with open('XDOSFUNCS.SRC', 'w') as outfile:
        xdosTable = open('XDOS_jumpTable.bin', 'rb').read(NUM_FUNCS*2)
        for funcNo in range(NUM_FUNCS):
            funcAddress, = unpack('<H', xdosTable[2*funcNo:2*funcNo+2])
            print(f"xdos{funcNo}: EQU 0{funcAddress:04X}H; Crystal DOS Function {funcNo:2}",
                  file=outfile)
if __name__=="__main__":
    main()
