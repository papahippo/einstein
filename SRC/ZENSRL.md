`ZENSRL.SRC` is a collection of patches and additions to the ZEN
editor to support reading and writing source and object code to and from
a serial port.

Much of this code is going to be superseded by the code in 'XXDOS.SRC'
(described in `XXDOS.md`) when I get that up and running so I won't spent 
many words on explanation here!

 