`MSXMOUSE.SRC` contains low level driver code (and a rather too minimal
test program) for an MSX mouse connected to the USER IO port as
follows:

- MSX pins 1-4  =>  Ein UIO pins 2,4,6,8 = up, down, left, right
- MSX pins 5    <=  Ein UIO pin 15 = +5volts
- MSX pins 6,7  =>  Ein UIO pins 10, 12 = buttons 1,2
- MSX pin 8     <=  Ein UIO pin 14 = digit select
- MSX pin 9    <=>  Ein UIO pin 9 = ground

``