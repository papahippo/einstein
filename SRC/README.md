The first 'source' file I chose to upload from 3" disk to this project is a pure matter of nostalgiia:

`LM9101C.SRC` is the first invoice I ever sent out after starting
my freelance software business in Eindhoven.
 
But the rest of the .SRC files here are genuine Z80A assembler source
files, a understood by the wonderful ZEN1.0 editor:assembler.

I have renamed many of these files from the names on my old 3"
disks;  I had used numbers at the
end of the filename stem to represent ad hoc version numbers.
ZEN doesn't let you reuse save file names. (and going into XDOS
just to delete the existing file first was too much of a faff!)
I am however sticking with the old-fashioned use
of upper case, to avoid potential problems when downloading files
back to Einstein. 

I have created separate *.md files for many of the source files. I find this
preferable to including detailed documentation within the soruce files;
rememmber those 3" disks hold less than 200kbytes per side.

`ROMCOP3.SRC` is my first ever Einstein source file creation - actually
largely written by my old colleague and (still) brother-in-law-in-law
(sic) Dave Horner. This program copies the 8kbytes of Einsteins ROM into
the higher 32kbytes of memory, so that it can then be saved and
disassembled. At the time, was knew there was a lot of powerful
stuff in the ROM but didn't know how to get it working to
our advantage. In particular we 'cracked' the mysterious RST 38H
instructions. I have recently added a few comments to this file. 

Somewhere I have a Basic(!) program which ananlyses the
above-mentioned table and prints out the table of addresses of the
various RST38H MCAL functions... ...but I haven't tracked it
down and uploaded it yet. (I presumably hadn't cracked the output
formatting possibiilies within either Einstein's ROM or within ZEN
in order to do this in assembler.)

`MSXMOUSE.SRC` contains low level driver code (and a rather too minimal
test program) for an MSX mouse connected to the USER IO port as
follows:

- MSX pins 1-4  =>  Ein UIO pins 2,4,6,8 = up, down, left, right
- MSX pins 5    <=  Ein UIO pin 15 = +5volts
- MSX pins 6,7  =>  Ein UIO pins 10, 12 = buttons 1,2
- MSX pin 8     <=  Ein UIO pin 14 = digit select
- MSX pin 9    <=>  Ein UIO pin 9 = ground

``