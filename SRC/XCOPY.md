`READXDIR.SRC` is is really a latter-day XDOS (thus
essentially CP/M) utilty for reading the directory contents
of an Einstein (side of!) 3" disk.

It is currently 'hard-wired'  to always do (effectively)
`DIR *.*` or should  I say in traditional CP/M speak
`DIR ????????.???`.
Even imagining it ever being made more generic, its only
real points of added value with respect to
the standard Einstein 'dir' command are:

- it doesn't object to drive numbers greater than 3, so it
can work with my `xXDOS`. This is indeed why it
was written.

- It displays the size of each file; this is expressed in
hexadecimal as a count of 128-byte blocks, so arguably only
compatible with old-fashioned computer geeks!
