This information relates to the source file
`MS3MOUSE.SRC`. I prefer to keep this separate from
the actual source code, largely because I do a lot
of 'native' assembling and excess text can become a
burden.

This mini-driver was actually written to support
the Philips 'Computer Plus' SBC 7603 trackball.
The very little documentation that was supplied
with this is still in my possession... but it didn't
help much - doesn't name a protocol or even tell
me the baud rate or even if it uses serial transfer
(as opposed to the strobing protocol of the MSX mouse
 which uses a similar physical connector).
 
Experimentation and Google established that it
uses the Microsoft 3-byte mouse protocol - see
https://linux.die.net/man/4/mouse
with perhaps 
a minor variation. Hence is expects to be connected to an
RS232 port with the setting: 1200 baud, 7 data bits,
no parity, one stop bit.

It took me a long time to draw the above conclusion,
partly because one of the internal springs 'had sprung'
(now fixed), preventing all reporting of
X movement - and it also took me a while to realise that
7N1 really needs to be read as such; reading it as e.g. 8N1
'for now' doesn't just shift the data a little - it really
messes up the framing big-time!

Testing with 'minicom' on a (Linux of course!) PC configured for
1200 7N1, word wrap enabled, hex display enabled, revealed
the following correspondence between actions and
frames sent:
  
- 60 00 00 on left button pressed
- 40 00 00 on left button released
- 40 00 00 on middle button pressed (!)
- 40 00 00 on middle button released
- 50 00 00 on right button pressed
- 40 00 00 on right button released
- 4c 01 3e   4c 00 3e  ... on movement up
- 40 00 2f   40 00 1e  ... on movement down
- 43 3f 00   4f 3f 3f  ... on movement left
- 40 2b 00   40 25 00  ... on movement right

Note that '40 00 00' can mean either "last button released"
or "middle button pressed"; My mini-driver currently always
assumes the latter (less likely!) case; something
to fix when I've got the basics working.

Alas, I have experienced a number of problems with the data
connection between the trackball and Einstein. The clever 'domino five'
DIN connector on Einstein means that in principle you can switch beween
DTE and DCE configuration simply by inverting the plug. But the plug
only provides one handshake signal in each direction called RTS and CTS.
For correct operation according to spec, Einstein's RTS (output) needs to be
connected both to the RTS and to the DTR (both input). I had initially not
connected the DTR. Only when I shorted the DTR to the CTS did the
trackball start to transmit. Even so, the signal looked rather flaky
on teh scope and Einstein didn't see the incoming bytes. The voltage on 
Einstein's RTS output dropped from 12v to 5v when the trackball was connected;
perhaps if I bring Einstein's (alleged!) buffered DTR signal to teh outside 
world and connect that up... no! it's too scary for me!  I'm reluctant
to mess about any mre with the serial interface; I alternatively 
use this for a PC link to upload and download files so don't want to
break it beyond redemption!.
So I have put this problem on hold for now.

In the meantimne, I have verified the mini-driver by connecting to minicom and
sending the bytes one by one. (An almost unhealthy knowledge of ASCII control
codes set me in good stead here!)
