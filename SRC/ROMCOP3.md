`ROMCOP3.SRC` is my first ever Einstein source file creation - actually
largely written by my old colleague and (still) brother-in-law-in-law
(sic) Dave Horner. This program copies the 8kbytes of Einsteins ROM into
the higher 32kbytes of memory, so that it can then be saved and
disassembled. At the time, was knew there was a lot of powerful
stuff in the ROM but didn't know how to get it working to
our advantage. In particular we 'cracked' the mysterious RST 38H
instructions. I have recently added a few comments to this file. 

Somewhere I have a Basic(!) program which analyses the
above-mentioned table and prints out the table of addresses of the
various RST38H MCAL functions... ...but I haven't tracked it
down and uploaded it yet. (I presumably hadn't cracked the output
formatting possibiilies within either Einstein's ROM or within ZEN
needed to do this in assembler.)
