`XXDOS.SRC` contains extension extensiosn to Crystal Research's XDOS
to support virtual drives hosted by a server on a PC, connected by
the RS232C serial port.

XDOS calls to disk devices which relate to a drive numer of 5 
( XDOS numbering - i.e. 4 Einstein numbering!) or more are
presumed to relate to a virtual disk relayed to the server, together
with data if appropriate. The server responds with a completion code
and data as appropriate.

This progam also provdies the option to modifiy the very
first sector of the XDOS image and writes it back to
(Einstein) drive zero.

Currently, this works within teh protactive environment of ZEN.
This is good enough for my own immediate requirements.

